<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function show()
    {
 		return response()->download(storage_path('app/IDAP_Profile.pdf'), 'IDAP_Profile');
    }

    public function create(Request $request)
    {
    	$path = $request->file('photo')->store('testing');
    	return response()->json(['path' => $path], 200);
    }
}
