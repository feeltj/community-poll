Laravel is one of the most popular PHP frameworks for building elegant applications. In this course, learn how to build a RESTful API using Laravel. Instructor Justin Yost goes over some of the basic pieces of the framework, and then covers how to build a browse, read, edit, add, and delete (BREAD) API in Laravel. He then shares how to customize your API, including how to load related data or subresources for a primary record, return nested data, and create a logging and rate limit middleware. He also explores how to use Behat to test your API and ensure that it works as expected.
Learning Objectives

    Reviewing the basics of an API
    Basic pieces of the Laravel framework
    Reading, adding, editing, and deleting a record
    API errors and exceptions
    Transforming your API data
    Returning nested and sideloaded data
    Creating a logging middleware
    Authenticating to your API
    Basic and advanced endpoint testing

