Feature: Samplee Tests
In Order the test the API
I need to be able to test the API

Scenario: Get Questions
Given I have the payload:
"""
"""
When I request "GET /api/questions"
Then the reponse is JSON
Then the reponse contains a question

Scenario: Add Question
Given I have the payload:
"""
{
	"title": "Behat",
	"question": "Is is awesome",
	"poll_id": 2
}
"""
When I request "POST /api/questions"
Then the reponse is JSON
Then the question contains a title of "Behat"
