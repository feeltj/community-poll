<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->bearerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNiZDdjODk2MmE4MGM0NTE3ZjUyNjUxOTcxOWRkM2QzYjYyOWY0MzkwOTc2NDc0MWQxY2M4YmZlZmZiMDQzNTIxZTBiZDlmZjU4YTVlNWZjIn0.eyJhdWQiOiIzIiwianRpIjoiY2JkN2M4OTYyYTgwYzQ1MTdmNTI2NTE5NzE5ZGQzZDNiNjI5ZjQzOTA5NzY0NzQxZDFjYzhiZmVmZmIwNDM1MjFlMGJkOWZmNThhNWU1ZmMiLCJpYXQiOjE1NzcxNTkxNjksIm5iZiI6MTU3NzE1OTE2OSwiZXhwIjoxNjA4NzgxNTY5LCJzdWIiOiIyMSIsInNjb3BlcyI6W119.ZplwuwHoO7cbyK__Pxw5uHGZQfZaBeRRVvqB1t8co5aN3IflgX9eeoGekxEcdmleeuKwCQjBq0ihvdAoWjxVUMFOhzk8gFRKq4MvVplZOiUhD7m3AAFMK2A3QQWbcbKmXdEv31XzdUMHpZPbTfbpn73mASI-AdWTw_pIVPZTEI8UA1goOGcS1OLr2q9yrt7Ysu5YeLQCBpQcQ2njvEesepYnwESW0Kfhuj8YPLhO6-4SdvU4Uyt-57ZK87ef0BUYCRjbnNV0scXf__T3TQYZjYMfiVAJieONDyQMLt8ORVMRfKXPvW2BfVncJ1WIQGgRGNX2qRllDp93Vg8vNBrXKJzV7Y7xxTwe3cLcenzPaKG6bvD2hKG2wGstaKpdAqjUvXQ5niv1X_k_rR6B3WdFGTX_hbh0x5f772tJGeA5y1vybOV8V8SvJrQfSSf_6x74NrOnRetluMPs_FR0m4T7SoQNffRUeyCZf8uPuO7CO2sgb_I06bapyQlCW6u5GCinT4pdKy8y2Zc3raDIscOrch4erPvsF9PNoqajoD5Mgd-6PwIrQo-_jnXQDrH_6RpcZK3IrshBPQKHFbEHTTFZrHQAY5wndKGAbcNAM74bcikvcmxbhvKaLIWW3ASTfyVDRwavO9P1AQwmfzD1Is3jFCtJcVBWQtNStDBZkPGIgXY";
    }

    /**
     * @Given I have the payload:
     */
    public function iHaveThePayload(PyStringNode $string)
    {
        $this->payload = $string;
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE|PATCH) ([^"]*)"$/
     */
    public function iRequest($httpMethod, $argument1)
    {
        $client = new GuzzleHttp\Client();
        $this->response = $client->request(
            $httpMethod,
            'http://127.0.0.1:8000' . $argument1,
            [
                'body' => $this->payload,
                'headers' => [
                    "Authorization" => "Bearer {$this->bearerToken}",
                    "Content-Type" => "application/json",
                ],
            ]
        );
        $this->responseBody = $this->response->getBody(true);
    }

    /**
     * @Then /^I get a response$/
     */
    public function iGetAResponse()
    {
        if (empty($this->responseBody)) {
            throw new Exception('Did not get a response from the API');
        }
    }

    /**
     * @Given /^the response is JSON$/
     */
    public function theResponseIsJson()
    {
        $data = json_decode($this->responseBody);

        if (empty($data)) {
            throw new Exception("Response was not JSON\n" . $this->responseBody);
        }
    }
    /**
     * @Then the reponse contains is :arg1 records
     */
    public function theReponseContainsIsRecords($arg1)
    {
        $data = json_decode($this->responseBody);
        $count = count($data);
        return ($count == $arg1);
    }

    /**
     * @Then the question contains a title of :arg1
     */
    public function theQuestionContainsATitleOf($arg1)
    {
        $data = json_decode($this->responseBody);
        if($data->title == $arg1) {

        } else {
            throw new Exception("The title does not match");
        }
    }
}